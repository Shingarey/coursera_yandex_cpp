#include <string>
#include <iostream>
#include <cassert>
#include <vector>
#include <map>

using namespace std;

enum class QueryType {
  NewBus,
  BusesForStop,
  StopsForBus,
  AllBuses
};

struct Query {
  QueryType type;
  string bus;
  string stop;
  vector<string> stops;
};

istream& operator >> (istream& is, Query& q) {
  // Реализуйте эту функцию
    string operation_code;
    cin >> operation_code;

    if (operation_code == "NEW_BUS") {
        q.type = QueryType::NewBus;
        cin >> q.bus;
        int stop_count;
        cin >> stop_count;
        q.stops.resize(stop_count);
        for (string& stop : q.stops) {
            cin >> stop;
        }
      } else if (operation_code == "BUSES_FOR_STOP") {
          q.type = QueryType::BusesForStop;
          cin >> q.stop;
      } else if (operation_code == "STOPS_FOR_BUS") {
        q.type = QueryType::StopsForBus;
          cin >> q.bus;
      } else if (operation_code == "ALL_BUSES") {
        q.type = QueryType::AllBuses;
      }
          return is;
}

struct BusesForStopResponse {
  // Наполните полями эту структуру
    vector<string> buses;
};

ostream& operator << (ostream& os, const BusesForStopResponse& r) {
    if (r.buses.size() == 0)
      {
          os << "No stop";
      }
      else
      {
          for (const string &bus : r.buses)
          {
              os << bus << " ";
          }
          os;
      }
  return os;
}

struct StopsForBusResponse {
  // Наполните полями эту структуру
  vector<string> stops;
  vector<vector<string>> buses;
  string bus;
};

ostream& operator << (ostream& os, const StopsForBusResponse& r) {
  // Реализуйте эту функцию
      if (r.stops.size() == 0) {
        os << "No bus";
      } else {
        for (size_t i = 0; i < r.stops.size(); i++)
        {
          os << "Stop " << r.stops[i] << ": ";
          if (r.buses[i].size() == 1) {
            os << "no interchange";
          } else {
            for (const string& other_bus : r.buses[i]) {
                if (other_bus != r.bus)
                {
                    os << other_bus << " ";
                }
            }
          }
          if (i < (r.stops.size() - 1))
          {
            os << endl;
          }

        }
        }
  return os;
        //for (const string& stop : r.stops) {
        //  os << "Stop " << stop << ": ";
        //  if (r.stops.size() == 1) {
        //    os << "no interchange";
        //  } else {
        //    for (const string& other_bus : r.stops) {
        //        os << other_bus << " ";
        //    }
        //  }
        //  os << endl;
        //}
}

struct AllBusesResponse {
  // Наполните полями эту структуру
  vector<string> buses;
  vector<vector<string>> stops;
  //map<string, vector<string>> all_buses;
};

ostream& operator << (ostream& os, const AllBusesResponse& r) {
  // Реализуйте эту функцию
      if (r.buses.empty()) {
        os << "No buses";
      } else {
        for (size_t i = 0; i < r.buses.size(); i++)
        {
          os << "Bus " << r.buses[i] << ": ";
          //for (size_t k = 0; k < r.stops[i].size(); k++)
          //{
          //  os << r.stops[i][k] << " ";
          //}
          for (const string& stop : r.stops[i]) {
            os << stop << " ";
          }
          if (i < r.buses.size() -1)
          {
                      os << endl;
          }

        }
      }
  return os;
}

class BusManager {
public:
  void AddBus(const string& bus, const vector<string>& stops) {
      for (const string &stop : stops)
      {
          stops_to_buses[stop].push_back(bus);
          buses_to_stops[bus].push_back(stop);
      }
  }

  BusesForStopResponse GetBusesForStop(const string& stop) const {
      vector<string> buses;
      if (stops_to_buses.count(stop) != 0) {
        for (const string &bus : stops_to_buses.at(stop))
        {
            buses.push_back(bus);
        }
      }
      return {buses};

  }

  StopsForBusResponse GetStopsForBus(const string& bus) const {
      vector<string> stops;
      vector<vector<string>> buses;
      if (buses_to_stops.count(bus) != 0) {
        for (const string& stop : buses_to_stops.at(bus)) {
                stops.push_back(stop);
                buses.push_back(GetBusesForStop(stop).buses);
            }
        }
    return {stops, buses, bus};
  }

  AllBusesResponse GetAllBuses() const {
      vector<string> buses;
        vector<vector<string>> stops;
    for (const auto& bus_item : buses_to_stops){
        buses.push_back(bus_item.first);
        stops.push_back(bus_item.second);
    }
      return {buses, stops};
  }

  private:
      map<string, vector<string>> buses_to_stops, stops_to_buses;

};

// Не меняя тела функции main, реализуйте функции и классы выше

int main() {
  int query_count;
  Query q;

  cin >> query_count;

  BusManager bm;
  for (int i = 0; i < query_count; ++i) {
    cin >> q;
    switch (q.type) {
    case QueryType::NewBus:
      bm.AddBus(q.bus, q.stops);
      break;
    case QueryType::BusesForStop:
      cout << bm.GetBusesForStop(q.stop) << endl;
      break;
    case QueryType::StopsForBus:
      cout << bm.GetStopsForBus(q.bus) << endl;
      break;
    case QueryType::AllBuses:
      cout << bm.GetAllBuses() << endl;
      break;
    }
  }
  }