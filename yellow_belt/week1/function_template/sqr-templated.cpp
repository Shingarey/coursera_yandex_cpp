#include <vector>
#include <map>
#include <vector>
#include <iostream>
using namespace std;

template <typename T>
vector<T> operator*(vector<T> &v_lhs, vector<T> &v_rhs);

template <typename first, typename second>
pair<first, second> operator*(pair<first, second> &p_lhs, pair<first, second> &p_rhs);

template <typename T, typename first, typename second>
map<T, pair<first, second>> operator*(map<T, pair<first, second>> &m, map<T, pair<first, second>> &m2);

template <typename T, typename K>
map<T, K> operator*(map<T, K> &m, map<T, K> &m2);

template <typename Collection>
Collection Sqr(Collection collection);

template <typename T>
vector<T> operator*(vector<T> &v_lhs, vector<T> &v_rhs)
{
    for (T &el : v_lhs)
    {
        el = el * el;
    }
    return v_lhs;
}

template <typename first, typename second>
pair<first, second> operator*(pair<first, second> &p_lhs, pair<first, second> &p_rhs)
{
    return {{p_lhs.first * p_lhs.first}, {p_lhs.second * p_lhs.second}};
}

template <typename T, typename first, typename second>
map<T, pair<first, second>> operator*(map<T, pair<first, second>> &m, map<T, pair<first, second>> &m2)
{
    for (auto &[key, value] : m)
    {
        value = value * value;
    }
    return m;
}

template <typename T, typename K>
map<T, K> operator*(map<T, K> &m, map<T, K> &m2)
{
    for (auto &[key, value] : m)
    {
        value = value * value;
    }
    return m;
}

template <typename Collection>
Collection Sqr(Collection collection)
{
    return collection * collection;
}

int main(int argc, char const *argv[])
{
    // Пример вызова функции
    cout << Sqr(2) << endl;
    vector<pair<int, int>> v_pair = {{1, 2}, {2, 3}};
    for (const auto &x : Sqr(v_pair))
    {
        cout << x.first << ' ' << x.second << endl;
    }

    map<int, pair<int, int>> map_of_pairs = {
        {4, {2, 2}},
        {7, {4, 3}}};
    cout << "map of pairs:" << endl;
    for (const auto &x : Sqr(map_of_pairs))
    {
        cout << x.first << ' ' << x.second.first << ' ' << x.second.second << endl;
    }

    pair<double, int> p = {2.0, 3};
    cout << Sqr(p).first << endl;

    map<double, double> m = {{4.0, 5.0}};
    cout << Sqr(m)[4.0] << endl;

    vector<int> v = {1, 2, 3};
    cout << Sqr(v)[2] << endl;

    map<int, vector<int>> arr1 = {{1, {2, 3}}};
    cout << Sqr(arr1).size() << endl;

    map<pair<int, int>, vector<int>> arr12 = {{{1, 2}, {3, 4, 5}}};
    cout << Sqr(arr12).size() << endl;

    map<map<int, int>, int> arr13 = {{{{1, 2}}, 4}};
    cout << Sqr(arr13).size() << endl;

    vector<pair<int, int>> arr2 = {{1, 2}};
    arr2 = Sqr(arr2);
    cout << arr2[0].second << endl;

    vector<map<int, int>> arr3 = {{{1, 1}}};
    cout << Sqr(arr3).size() << endl;

    vector<vector<int>> arr31 = {{1, 2}, {3, 4}};
    cout << Sqr(arr31)[0][0] << endl;

    pair<int, vector<map<int, int>>> arr4 = {3, {{{1, 2}}}};
    cout << Sqr(arr4).first << endl;

    pair<map<int, int>, vector<int>> arr41 = {{{2, 3}}, {4, 5}};
    cout << Sqr(arr41).second[0] << endl;
    return 0;
}