#include <vector>
#include <map>
#include <string>
#include <iostream>
using namespace std;

template <typename KEY, typename VALUE>
VALUE &GetRefStrict(map<KEY, VALUE> &m, KEY k)
{
    if (m.count(k) != 0)
    {
        return m[k];
    }
    throw(runtime_error(""));
}

int main(int argc, char const *argv[])
{
    map<int, string> m = {{0, "value"}};
    string &item = GetRefStrict(m, 0);
    item = "newvalue";
    cout << m[0] << endl; // выведет newvalue
    string &item2 = GetRefStrict(m, 3);
    return 0;
}
