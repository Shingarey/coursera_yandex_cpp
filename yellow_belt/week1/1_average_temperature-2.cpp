#include <vector>
#include <iostream>
#include <limits>

using namespace std;

long FindAverage(const vector<long> &v)
{
    long sum = 0;
    for (auto el : v)
    {
        sum = sum + el;
    }
    return (sum / static_cast<int>(v.size()));
}

void FindIndAboveAverage(vector<long> &v_above, const vector<long> &v)
{
    long av = FindAverage(v);
    int i = 0;
    for (auto el : v)
    {
        if (el > av)
        {
            v_above.push_back(i);
        }
        ++i;
    }
}

void PrintVector(const vector<long> &v)
{
    for (auto el : v)
    {
        cout << el << " ";
    }
    cout << endl;
}

int main(int argc, char const *argv[])
{
    // cout << numeric_limits<long>::max() << endl;
    int v_size;
    cin >> v_size;
    vector<long> v(v_size, 0);
    for (auto &el : v)
    {
        cin >> el;
    }
    vector<long> v_above;
    FindIndAboveAverage(v_above, v);
    cout << v_above.size() << endl;
    PrintVector(v_above);
}
