#include <iostream>
#include <cstdint>
#include <limits>
#include <vector>

using namespace std;

struct Block
{
    uint16_t W{0};
    uint16_t H{0};
    uint16_t D{0};
};

unsigned long long CalculateBlockMass(vector<Block> blocks, uint16_t density)
{
    unsigned long long mass{0};
    for (auto &block : blocks)
    {
        mass += static_cast<long>(block.W * block.H) * static_cast<long>(block.D * density);
    }
    return mass;
}

int main(int argc, char const *argv[])
{

    uint32_t number_of_blocks;
    uint16_t density;
    cin >> number_of_blocks >> density;
    vector<Block> blocks(number_of_blocks);
    for (auto &block : blocks)
    {
        cin >> block.W >> block.H >> block.D;
    }
    cout << CalculateBlockMass(blocks, density) << endl;
    return 0;
}
