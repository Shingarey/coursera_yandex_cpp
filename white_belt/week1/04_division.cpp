#include <iostream>

int main (){
    int a, b;
    std::cin >> a >> b;
    if (b != 0){
        std::cout << std::abs(a/b) << std::endl;
    }
    else{
        std::cout << "Impossible" << std::endl;
    }
    return 0;
}