#include <iostream>
#include <string>
#include <vector>
//#include <algorithm>

int main(){
    std::string str1, str2, str3, min_str;
    std::cin >> str1 >> str2 >> str3;
    std::vector<std::string> string_vector = {str1, str2, str3};
    min_str = str1;
    for (auto str : string_vector) {
        if (str<min_str){
            min_str = str;
        }
    }
    std::cout << min_str << std::endl;

//    std::cout <<  *std::min_element( string_vector.begin(), string_vector.end() ) << std::endl;
    return 0;
}