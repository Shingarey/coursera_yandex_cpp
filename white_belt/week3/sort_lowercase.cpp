#include<vector>
#include<iostream>
#include<string>
#include <algorithm>    // std::sort
#include <cctype>

using namespace std;

bool myfunction (const string& l, const string& r) { 
    if (l.size() < r.size())
    {
        return true;
    }
    if (l.size() < r.size())
    {
        return false;
    }
    if (l.size() == r.size())
    {
        for (size_t k = 0; k < l.size(); k++)
        {
            if (tolower(l[k])<tolower(r[k])) return true;
            else if (tolower(l[k])>tolower(r[k])) return false;
            //return( tolower(i[k]) < tolower(j[k]) );
        }
    }
}

int main(int argc, char const *argv[])
{
    int nr_of_elements;
    string el;
    cin >> nr_of_elements;

    vector<string> v(nr_of_elements); 

    for (string& x : v) {
      cin >> x;
    }

    sort(v.begin(), v.end(), myfunction);

    //sort(begin(v), end(v),
    // // компаратор для сортировки — лямбда-функция, сравнивающая строки без учёта регистра
    // [](const string& l, const string& r) {
    //   // сравниваем лексикографически...
    //   return lexicographical_compare(
    //       // ... все символы строки l ...
    //       begin(l), end(l),
    //       // ... со всеми символами строки r ...
    //       begin(r), end(r),
    //       // ..., используя в качестве компаратора сравнение отдельных символов без учёта регистра
    //       [](char cl, char cr) { return tolower(cl) < tolower(cr); }
    //   );
    // }
    //);

    for (auto& el : v)
    {
        cout << el << " ";
    }
    cout << endl;
    

}