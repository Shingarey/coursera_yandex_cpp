#include<vector>
#include<iostream>

using namespace std;

int FindAverage(const vector<int>& v){
    int sum = 0;
    for (auto el : v)
    {
        sum = sum + el;
    }
    return ( sum / v.size() );   
}

void FindIndAboveAverage(vector<int>& v_above, const vector<int>& v){
    int av = FindAverage(v);
    int i = 0;
    for (auto el : v)
    {
        if (el > av)
        {
            v_above.push_back(i);
        }  
        ++i; 
    }
}

void PrintVector(const vector<int>& v){
    for (auto el : v)
    {    
        cout << el << " ";    
    }
    cout << endl;

}

int main(int argc, char const *argv[])
{
    int v_size;
    cin >> v_size;
    vector<int> v(v_size, 0); 
    for (int& el : v)
    {
        cin >> el;
    }
    vector<int> v_above;
    FindIndAboveAverage(v_above, v);
    cout << v_above.size() << endl;
    PrintVector(v_above);
}
