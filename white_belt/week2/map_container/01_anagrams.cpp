#include <iostream>
#include <map>
#include <string>
#include <vector>

using namespace std;

map<char, int> BuildCharCounter(string word)
{
    map<char, int> chars_quantity;
    for (auto char_ : word)
    {
        if (chars_quantity.count(char_) == 0)
        {
            chars_quantity[char_] = 1;
        }
        else
        {
            chars_quantity[char_] += 1;
        }
    }
    return chars_quantity;
}

int main(int argc, char const *argv[])
{
    int nr_of_operations;
    vector<map<char, int>> v1, v2;
    string word1, word2;
    cin >> nr_of_operations;
    for (size_t i = 0; i < nr_of_operations; i++)
    {
        cin >> word1 >> word2;
        v1.push_back(BuildCharCounter(word1));
        v2.push_back(BuildCharCounter(word2));
    }

    for (size_t i = 0; i < v1.size(); i++)
    {
        if (v1[i] != v2[i])
        {
            cout << "NO" << endl;
        }
        else
        {
            cout << "YES" << endl;
        }
    }

    return 0;
}
