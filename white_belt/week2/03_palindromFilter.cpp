#include <iostream>
#include <string>
#include <vector>

using namespace std;

bool IsPalindrom(string word){
    for (auto i = 0; i < word.size()/2; i++)
    {
        if (word[i] != word[word.size() - i - 1])
        {
            return false;
        }
    }
    return true;
}

vector<string> PalindromFilter(vector<string> words, int minLength){
    vector<string> filteredWords;
    for (auto s : words)
    {
        if ( (IsPalindrom(s)) && (s.size() >= minLength) ) { 
            filteredWords.push_back(s);
        }
    }
    return filteredWords;
}

int main(int argc, char const *argv[])
{
    /* code */
    vector<string> words = {"weew", "bro", "code", "abacaba", "aba"};
    for (auto s : PalindromFilter(words, 4))
    {   
       cout << s << endl;     
    }  
    return 0;
}
